# -*- encoding:utf-8 -*-
'''
@File       :   info.py
@Time       :   2021/2/10
@Author     :   HZM
@version    :   1.0
@Contact    :   3162840065@qq.com
'''

from lin.interface import InfoCrud
from sqlalchemy import Column, Integer, String
from app.exception.api import InfoNotFound


class Info(InfoCrud):
    '''
    define a database model of company info
    '''
    id = Column(Integer, primary_key=True, default=1)
    addr = Column(String(50), nullable=False)
    tel = Column(String(100), nullable=False)
    verification = Column(String(500), nullable=False)
    recordinfo = Column(String(5000), nullable=False)
    recordaddr = Column(String(100), nullable=False)