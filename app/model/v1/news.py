# -*- encoding: utf-8 -*-
'''
@File    :   news.py
@Time    :   2021/02/06 12:35:23
@Author  :   harry 
@Version :   1.0
@Contact :   834876096@163.com
'''

# here put the import lib

from lin.interface import InfoCrud as Base
from sqlalchemy import Column, Integer, String
from app.exception.api import NewsNotFound


class News(Base):
    '''
    define a database model of company news
    '''
    id = Column(Integer, primary_key=True, autoincrement=True)
    title = Column(String(50), nullable=False)
    content = Column(String(10000),nullable=False)
