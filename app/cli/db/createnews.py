from lin.db import db

from app.model.v1.news import News

def createnews():
    with db.auto_commit():
        # 批量添加新闻
        for i in range(10, 30):
            news = News()
            news.title = "title"+str(i)
            news.content = "content"
            db.session.add(news)
