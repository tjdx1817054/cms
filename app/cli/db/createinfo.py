from lin.db import db

from app.model.v1.info import Info

def createinfo():
    with db.auto_commit():
        # 添加公司信息
        info = Info()
        info.tel = "公司电话"
        info.verification = "公司简介"
        info.addr = "公司地址"
        info.recordinfo = "公司备案信息"
        info.recordaddr = "公司备案信息地址"
        db.session.add(info)

