from lin.apidoc import BaseModel

class NewsQuerySearchSchema(BaseModel):
    q: str


class NewsSchema(BaseModel):
    title: str
    content: str
