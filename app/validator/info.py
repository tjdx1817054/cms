# -*- encoding:utf-8 -*-
'''
@File       :   info.py
@Time       :   2021/2/10
@Author     :   HZM
@version    :   1.0
@Contact    :   3162840065@qq.com
'''

from lin.apidoc import BaseModel

# 信息类，add为地址，tel为电话，verification为简介，备案信息号
class InfoSchema(BaseModel):
    addr: str
    tel: str
    verification: str
    recordinfo : str
    recordaddr: str