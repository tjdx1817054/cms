# -*- encoding:utf-8 -*-
'''
@File       :   info.py
@Time       :   2021/2/10
@Author     :   HZM
@version    :   1.1
@Contact    :   3162840065@qq.com
'''

from lin.redprint import Redprint
from lin.exception import NotFound
from app.model.v1.info import Info
from flask import g
from lin.apidoc import api
from app.validator.info import InfoSchema
from flask import request
from lin.exception import Success
# 定义权限管理
from lin.jwt import admin_required, get_tokens, login_required

info_api = Redprint('info')

# 获取信息
@info_api.route('/<int:id>')
def get_info(id):
    # 通过Info模型在数据库中查询id=id，且没有被软删除的记录
    info = Info.query.filter_by(id=id, delete_time=None).first()
    if info:
        # 如果存在，则返回info的信息
        return info
    raise NotFound("暂无公司信息")

# 更新信息
@info_api.route("", methods=['PUT'])
# 加入权限管理，仅管理员可操作
@admin_required
@api.validate(json=InfoSchema)
def update_info():
    info = Info.get(id=1)
    # 请求体的json数据位于request.context.json
    info_schema = request.context.json
    if info:
        info.update(id=1, **info_schema.dict(), commit=True)
        # 2是更新成功的消息码
        return Success(16)
    raise NotFound(10020)