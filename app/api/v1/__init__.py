from flask import Blueprint
# from app.api.v1 import book
# 导入news的表结构 
from app.api.v1 import news 
# 导入info的表结构 
from app.api.v1 import info


def create_v1():
    bp_v1 = Blueprint('v1', __name__)
    # book.book_api.register(bp_v1)
    news.news_api.register(bp_v1)
    info.info_api.register(bp_v1)
    return bp_v1
