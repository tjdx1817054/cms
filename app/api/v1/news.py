# -*- encoding: utf-8 -*-
'''
@File    :   news.py
@Time    :   2021/02/11 10:48:33
@Author  :   harry 
@Version :   1.0
@Contact :   834876096@163.com
'''

# here put the import lib

from lin.redprint import Redprint
from lin.exception import NotFound
from app.model.v1.news import News
# from app.validator.book import BookQuerySearchSchema
from app.validator.news import NewsQuerySearchSchema
from flask import g
from lin.apidoc import api
from app.validator.news import NewsSchema
from flask import request
from lin.exception import Success
# 定义权限管理
from lin.jwt import admin_required, get_tokens, login_required

news_api = Redprint('news')


@news_api.route('/<int:id>')
# # 加入权限管理，仅管理员  
def get_new(id):
    # 通过News模型在数据库中查询id=`id`, 且没有被软删除的书籍
    news = News.query.filter_by(id=id, delete_time=None).first()
    if news:
        return news # 如果存在，返回该数据的信息
    raise NotFound('没有找到相关新闻') # 如果书籍不存在，返回一个异常给前端
 

@news_api.route('/search', methods=['GET'])
# 使用校验，需要引入定义好的对象`api`,它是Spectree的一个实例
# query代表来自url中的参数，如`http://127.0.0.1:5000?q=abc&page=1`中的 `q` 和 `page`都属于query参数
@api.validate(query=NewsQuerySearchSchema)
def search_news():
    # 使用这种方式校验通过的参数将会被挂载到g的对应属性上，方便直接取用。
    q = '%' + g.q + '%' # 取出参数中的`q`参数，加`%`进行模糊查询
    news = News.query.filter(News.title.like(q)).all() # 搜索书籍标题
    if news:
        return news
    raise NotFound('没有找到相关新闻')


# 新建新闻
@news_api.route("", methods=["POST"])
# json代表来自请求体body中的参数
@api.validate(json=NewsSchema)
@admin_required
def create_news():
    # 请求体的json 数据位于 request.context.json
    news_schema = request.context.json
    News.create(**news_schema.dict(), commit=True)
    # 12 是 消息码
    return Success(12)


# 更新新闻
@news_api.route("/<int:id>", methods=["PUT"])
@admin_required  
@api.validate(json=NewsSchema)
def update_news(id: int):
    news_schema = request.context.json
    news = News.get(id=id)
    if news:
        news.update(
            id=id,
            **news_schema.dict(),
            commit=True,
        )
        return Success(13)
    raise NotFound(10020)


# 删除新闻
@news_api.route("/<int:id>", methods=["DELETE"])
@admin_required  
def delete_news(id: int):
    """
    传入id删除对应新闻
    """
    news = News.get(id=id)
    if news:
        # 删除新闻，软删除
        news.hard_delete(commit=True)
        return Success(14)
    raise NotFound(10020)



# 获取所有新闻
@news_api.route("")
def get_news():
    """
    获取新闻列表
    """
    return News.get(one=False)
